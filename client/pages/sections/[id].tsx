import { SectionPage } from "@/pages/Section";

export { sectionPageGetStaticProps as getStaticProps } from "@/pages/Section";
export { sectionPageGetStaticPaths as getStaticPaths } from "@/pages/Section";
export default SectionPage;
