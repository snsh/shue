import { Home } from "@/pages/Home";
import { homeGetStaticProps as getStaticProps } from "@/pages/Home";

export { getStaticProps };
export default Home;
