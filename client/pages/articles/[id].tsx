import { ArticlePage } from "@/pages/Article";

export { articlePageGetStaticProps as getStaticProps } from "@/pages/Article";
export { articlePageGetStaticPaths as getStaticPaths } from "@/pages/Article";
export default ArticlePage;
