import { SubsectionPage } from "@/pages/Subsection";

export { subsectionPageGetStaticProps as getStaticProps } from "@/pages/Subsection";
export { subsectionPageGetStaticPaths as getStaticPaths } from "@/pages/Subsection";
export default SubsectionPage;
