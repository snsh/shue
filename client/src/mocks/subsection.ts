import { Article } from "@/entities/article";
import { SubsectionWithItems } from "@/entities/subsection";

export const mock_subsections: SubsectionWithItems<
  Pick<Article, "id" | "title">
>[] = [
  {
    id: "seed1243",
    title: "Загадочные тайны",
    description:
      " красочный закат, окрашивающий горы в оттенки розового и фиолетового. Лучи солнца играют на вершинах гор, создавая волшебную атмосферу. Под этим закатом можно погрузиться в размышления и насладиться моментом, полным спокойствия и гармонии",
    articles: [
      {
        id: "1",

        title: "Врачебная профессия",
      },

      {
        id: "2",
        title: "Специальности в медицине",
      },

      {
        id: "3",
        title: "Первая помощь",
      },

      {
        id: "4",
        title: "Медицинские исследования",
      },

      {
        id: "5",
        title: "Лекарственные средства",
      },
    ],
    qandas: [],
    supcenters: [],
  },
];
