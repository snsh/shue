import axios from "axios";
import getConfig from "next/config";

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export const apiUrl = serverRuntimeConfig.apiUrl || publicRuntimeConfig.apiUrl;

export const $api = axios.create({
  baseURL: apiUrl,
  // headers: {
  //   "Content-Type": "application/json",
  // },
});

$api.interceptors.request.use((config: any) => {
  config.headers = config.headers ?? {};
  if (typeof window !== "undefined" && localStorage.getItem("token")) {
    config.headers["Authorization"] = `Bearer ${localStorage.getItem("token")}`;
  }
  return config;
});

export default $api;
