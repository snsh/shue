import React from "react";
import { SnackBar } from "@consta/uikit/SnackBar";
import { createSelector, createSlice } from "@reduxjs/toolkit";
import {
  createBaseSelector,
  registerSlice,
  useAppDispatch,
  useAppSelector,
} from "../lib/redux";

type Item = {
  key: number;
  message?: string;
  status: "success" | "warning";
};

const initialState = {
  items: [] as Item[],
};

const snacbarSlice = createSlice({
  name: "snackbar",
  initialState,
  reducers: {
    handleAdd(state, action) {
      const key = state.items.length + 1;
      const item: Item = {
        key,
        message: action.payload.message,
        status: action.payload.status,
      };
      state.items.push(item);
    },
    handleRemove(state, action) {
      state.items = state.items.filter((item) => {
        return item.key != action.payload.key;
      });
    },
  },
});

const baseSelector = createBaseSelector(snacbarSlice);
registerSlice([snacbarSlice]);

export const snackbarStore = {
  actions: {
    ...snacbarSlice.actions,
  },
  selectors: {
    getItems: createSelector(baseSelector, (state) => state.items),
  },
};

export const MySnackBar = ({ className }: { className?: string }) => {
  const items = useAppSelector(snackbarStore.selectors.getItems);
  const dispatch = useAppDispatch();
  return (
    <SnackBar
      className={className ? className : ""}
      items={items}
      onItemClose={(item) => {
        dispatch(snackbarStore.actions.handleRemove({ key: item.key }));
      }}
      getItemAutoClose={() => 7}
    />
  );
};
