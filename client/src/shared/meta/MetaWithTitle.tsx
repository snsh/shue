import Head from "next/head";
import React from "react";

export const MetaWithTitle = ({ title }: { title: string }) => {
  return (
    <Head>
      <title>{title}</title>
      <meta property="og:title" content={title} key="title" />
    </Head>
  );
};
