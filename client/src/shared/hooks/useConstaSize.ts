import { useEffect, useState } from "react";
import { window_sizes } from "../types/window_sizes";
import { useWindowDimensions } from "./useWindowDimensions";

function createSize(
  small: window_sizes | undefined,
  medium: window_sizes | undefined,
  big: window_sizes | undefined
) {
  return {
    small,
    medium,
    big,
  };
}

export function useConstaSize() {
  const window = useWindowDimensions();
  const width = window["width"] ? window["width"] : 0;

  if (width > 1024) {
    return createSize(window_sizes.S, window_sizes.M, window_sizes.L);
  }
  if (width > 600) {
    return createSize(window_sizes.XS, window_sizes.S, window_sizes.L);
  }
  return createSize(window_sizes.XS, window_sizes.XS, window_sizes.M);
}
