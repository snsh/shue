export enum window_sizes {
  L = "l",
  M = "m",
  S = "s",
  XS = "xs",
}
