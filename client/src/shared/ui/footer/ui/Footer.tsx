import React from "react";
import "./Footer.scss";
import Image from "next/image";
import { MySnackBar } from "@/shared/modules/Snackbar";

const Footer = () => {
  return (
    <div className="footer__container">
      <MySnackBar className="snackbar" />
      <Image
        src="/static/footer/footer.png"
        alt="футер"
        className="footer__img"
        fill={true}
      />
    </div>
  );
};

export { Footer };
