import React from "react";
import styles from "./style.module.scss";
import { style_decorator } from "@/shared/utils/css_module_decorator";
import { useRouter } from "next/router";

const gs = style_decorator(styles);
export const LogoHeader = () => {
  const router = useRouter();
  return (
    <div className={gs("root")}>
      <div className={gs("image")}>
        <img
          src="/static/logo/logo.svg"
          alt="logo"
          onClick={() => router.push("/")}
        />
      </div>
    </div>
  );
};
