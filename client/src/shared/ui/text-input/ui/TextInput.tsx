import React from "react";
import "./TextInput.scss";
import { TextField } from "@consta/uikit/TextField";
import { TextInputProps } from "../types";
import { check_classname } from "@/shared/utils/check_classname";

export const TextInput = ({
  className,
  children,
  ...props
}: TextInputProps) => {
  return (
    <TextField className={check_classname(className, "text-input")} {...props}>
      {children}
    </TextField>
  );
};
