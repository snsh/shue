import { TextFieldProps } from "@consta/uikit/TextField";

export type TextInputProps = TextFieldProps<"textarea" | "text" | "email">;
