import React from "react";
import "./card.scss";
import { check_classname } from "@/shared/utils/check_classname";

export type CardProps = React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>;

export const Card = ({ className, children, ...props }: CardProps) => {
  return (
    <div className={check_classname(className, `card`)} {...props}>
      {children}
    </div>
  );
};
