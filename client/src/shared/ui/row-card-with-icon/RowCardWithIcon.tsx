import React from "react";
import { Card, CardProps } from "@/shared/ui/card/Card";
import Link from "next/link";
import styles from "./row-card-with-icon.module.scss";
import { style_decorator } from "@/shared/utils/css_module_decorator";

const gs = style_decorator(styles);

export type RowCardWithIconProps = {
  text: string;
  href: string;
  Icon: React.JSX.Element;
} & CardProps;

export const RowCardWithIcon = ({ text, href, Icon }: RowCardWithIconProps) => {
  return (
    <Card className={gs("root")}>
      <div className={gs("root__wrapper")}>
        <div className={gs("img")}>{Icon}</div>
        <div className={gs("root__text text")}>
          <span>{text}</span>
        </div>
        <Link href={href} className={gs("root__link")} />
      </div>
    </Card>
  );
};
