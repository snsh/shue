import React from "react";
import { Link } from "../link/Link";
import styles from "./empty-list.module.scss";
import { style_decorator } from "@/shared/utils/css_module_decorator";

const gs = style_decorator(styles);

export const EmptyList = () => {
  return (
    <div className={gs("root")}>
      <h2 className={gs("root__title title")}>
        Информация о разделе не найдена
      </h2>
      <Link href="/" className={gs("root__link link")}>
        На главную
      </Link>
    </div>
  );
};
