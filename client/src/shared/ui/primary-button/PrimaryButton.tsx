import { Button, Props } from "@consta/uikit/Button";
import React from "react";
import "./primary-button.scss";
import { check_classname } from "@/shared/utils/check_classname";

export const PrimaryButton = ({ className, ...props }: PrimaryButtonProps) => {
  return (
    <Button
      className={check_classname(className, `primary-button`)}
      {...props}
    />
  );
};

export type PrimaryButtonProps = { className?: string } & Props;
