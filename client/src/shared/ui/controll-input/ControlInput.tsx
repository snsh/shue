import React from "react";
import { Controller, useFormContext } from "react-hook-form";

import { TextField } from "@consta/uikit/TextField";
import { Text } from "@consta/uikit/Text";

export const ControllInput = ({
  name,
  rules,
  defaultValue,
  control,
  shouldUnregister,
  ...textFieldProps
}: any) => {
  const renderTextField = ({ field, fieldState, formState }: any) => {
    const { onChange, onBlur, value, ref } = field;
    const handleChange = ({ e }: any) => {
      onChange(e);
    };
    return (
      <div>
        <TextField
          {...textFieldProps}
          onChange={handleChange}
          onBlur={onBlur}
          value={value}
          ref={ref}
        />
        {fieldState.error && (
          <Text view="alert">{fieldState.error.message}</Text>
        )}
      </div>
    );
  };

  return (
    <Controller
      name={name}
      rules={rules}
      defaultValue={defaultValue}
      control={control}
      shouldUnregister={shouldUnregister}
      render={renderTextField}
    />
  );
};
