import React from "react";
import { LinkProps, default as NextLink } from "next/link";
import "./link.scss";
import { check_classname } from "@/shared/utils/check_classname";
export const Link = ({
  href,
  className,
  children,
  ...props
}: LinkProps & { className?: string | null; children?: React.ReactNode }) => {
  return (
    <NextLink
      href={href}
      className={check_classname(className, `link`)}
      {...props}
    >
      {children}
    </NextLink>
  );
};
