import { MetaWithTitle } from "@/shared/meta/MetaWithTitle";
import { MySnackBar } from "@/shared/modules/Snackbar";

export function BasePageLayout({
  children,
  header,
  footer,
  pageTitle,
}: {
  pageTitle: string;
  children: React.ReactNode;
  header: React.ReactNode;
  footer: React.ReactNode;
}) {
  return (
    <>
      <MetaWithTitle title={pageTitle} />
      <div className="container">
        {header}
        {children}
      </div>
      {footer}
    </>
  );
}
