import { style_decorator } from "@/shared/utils/css_module_decorator";
import styles from "./grid-navbar.module.scss";
import { ReactNode } from "react";

const gs = style_decorator(styles);

export function GridNavbarLayout({
  navbarItems,
}: {
  navbarItems: React.ReactNode;
}) {
  return <div className={gs("root")}>{navbarItems}</div>;
}

export function GridNavbarItemDecorator({
  children,
}: {
  children: ReactNode;
}): ReactNode {
  return <div className={gs("root__item")}>{children}</div>;
}
