import React from "react";
import styles from "./layout.module.scss";
import { style_decorator } from "@/shared/utils/css_module_decorator";
import { EmptyList } from "../../empty-list/EmptyList";

const gs = style_decorator(styles);

export type childElement = {
  element: React.JSX.Element;
  id: string;
};

export type ItemWithChildrensLayoutProps = {
  title: string;
  description: string;
  childrenList: childElement[];
};
export const ItemWithChildrensLayout = ({
  title,
  description,
  childrenList,
}: ItemWithChildrensLayoutProps) => {
  return (
    <main className={gs("root")}>
      <div className={gs("root__item")}>
        <h2 className={gs("item__title title")}>{title}</h2>
        <p className={gs("item__description description")}>{description}</p>
      </div>
      {childrenList.length ? (
        <ul className={gs("root__list")}>
          {childrenList.map((item) => {
            return (
              <li key={item.id} className={gs("root__list__item list-item")}>
                {item.element}
              </li>
            );
          })}
        </ul>
      ) : (
        <EmptyList />
      )}
    </main>
  );
};
