import React from "react";
import { Theme, presetGpnDefault } from "@consta/uikit/Theme";

export function ThemeProvider({ children }: { children: React.ReactNode }) {
  return <Theme preset={presetGpnDefault}>{children}</Theme>;
}
