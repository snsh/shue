export function check_classname(
  className: string | undefined | null,
  otherClasses: string | undefined | null
) {
  return className ? `${className + " " + otherClasses}` : `${otherClasses}`;
}
