export function style_decorator(styles: { [key: string]: string }) {
  return function (classes: string) {
    return classes
      .split(" ")
      .map((cl) => {
        return styles[cl];
      })
      .join(" ");
  };
}
