import { RowCardWithIconProps } from "@/shared/ui/row-card-with-icon/RowCardWithIcon";

export type Article = {
  id: string;
  title: string;
  createdAt: string;
  content: string;
  keyWords: string[];
  author: string;
  description: string;
};

export type ArticleRowCardProps = Omit<RowCardWithIconProps, "Icon"> & {
  id?: string;
};
