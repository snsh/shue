import React from "react";
import { RowCardWithIcon } from "@/shared/ui/row-card-with-icon/RowCardWithIcon";
import { ArticleRowCardProps } from "../model";
import { ArticleRowCardIcon } from "./ArticleRowCardIcon";

export const ArticleRowCard = ({ text, href }: ArticleRowCardProps) => {
  return (
    <RowCardWithIcon text={text} href={href} Icon={<ArticleRowCardIcon />} />
  );
};
