import { useConstaSize } from "@/shared/hooks/useConstaSize";
import { window_sizes } from "@/shared/types/window_sizes";
import { IconBook } from "@consta/icons/IconBook";
import React, { useEffect, useState } from "react";

export const ArticleRowCardIcon = () => {
  const [size, setSize] = useState<window_sizes | undefined>(window_sizes.S);
  const constaSize = useConstaSize();

  useEffect(() => {
    setSize(constaSize.big);
  }, [constaSize]);
  return <IconBook size={size} />;
};
