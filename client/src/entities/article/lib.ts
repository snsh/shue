export const ARTICLES_URL = "articles";

export function getArticleHref(id: string) {
  return `/${ARTICLES_URL}/${id}`;
}
