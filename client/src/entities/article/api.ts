import $api from "@/shared/api/axios-instance";
import { Article } from "./model";

export async function getArticleById(id: string) {
  try {
    const response = await $api.get<Article>(`/articles/${id}`);
    return response;
  } catch (err: unknown) {
    throw err;
  }
}

export async function getAllArticles() {
  try {
    const response = await $api.get<Pick<Article, "id" | "title">[]>(
      `/articles`
    );
    return response;
  } catch (err: unknown) {
    throw err;
  }
}
