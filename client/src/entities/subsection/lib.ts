export const SUBSECTIONS_URL = "subsections";

export function getSubsectionHref(id: string) {
  return `/${SUBSECTIONS_URL}/${id}`;
}
