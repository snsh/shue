import { useConstaSize } from "@/shared/hooks/useConstaSize";
import { window_sizes } from "@/shared/types/window_sizes";
import { IconCards } from "@consta/icons/IconCards";
import React, { useEffect, useState } from "react";

export const SubsectionCardIcon = () => {
  const [size, setSize] = useState<window_sizes | undefined>(window_sizes.S);
  const constaSize = useConstaSize();

  useEffect(() => {
    setSize(constaSize.big);
  }, [constaSize]);
  return <IconCards size={size} />;
};
