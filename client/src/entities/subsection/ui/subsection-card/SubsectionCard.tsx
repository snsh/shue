import React from "react";
import { SubsectionCardIcon } from "./SubsectionCardIcon";
import { RowCardWithIcon } from "@/shared/ui/row-card-with-icon/RowCardWithIcon";
import { SubsectionCardProps } from "../../model";

export const SubsectionCard = ({ text, href }: SubsectionCardProps) => {
  return (
    <RowCardWithIcon text={text} href={href} Icon={<SubsectionCardIcon />} />
  );
};
