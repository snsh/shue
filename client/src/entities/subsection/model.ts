import { RowCardWithIconProps } from "@/shared/ui/row-card-with-icon/RowCardWithIcon";

export type Subsection = {
  id: string;
  title: string;
  description: string;
};

export type SubsectionWithItems<Items> = Subsection & {
  articles: Items[];
  qandas: Items[];
  supcenters: Items[];
};

export type SubsectionCardProps = Omit<RowCardWithIconProps, "Icon"> & {
  id?: string;
};
