import $api from "@/shared/api/axios-instance";
import { Subsection, SubsectionWithItems } from "./model";

export async function getAllSubsections() {
  try {
    const response = await $api.get<Pick<Subsection, "id" | "title">[]>(
      "/subsections"
    );
    return response;
  } catch (err: unknown) {
    throw err;
  }
}

export async function getSubsectionWithItemsById<T>(id: string) {
  try {
    const response = await $api.get<SubsectionWithItems<T>>(
      `/subsections/${id}`
    );
    return response;
  } catch (err: unknown) {
    throw err;
  }
}

export async function getRandomSubsections() {
  try {
    const response = await $api.get<Subsection[]>(`/random_subsections`);
    return response;
  } catch (err: unknown) {
    throw err;
  }
}
