export type Section = {
  id: string;
  title: string;
  description: string;
  image: string;
};

export type SectionWithItems<Items> = {
  subsections: Items[];
} & Omit<Section, "image">;
