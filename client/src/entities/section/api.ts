import $api from "@/shared/api/axios-instance";
import { Section, SectionWithItems } from "./model";

export async function getAllSections() {
  try {
    const response = await $api.get<Section[]>("/sections");
    return response;
  } catch (err: unknown) {
    throw err;
  }
}

export async function getSectionWithItemsById<T>(id: string) {
  try {
    const response = await $api.get<SectionWithItems<T>>(`/sections/${id}`);
    return response;
  } catch (err: unknown) {
    throw err;
  }
}
