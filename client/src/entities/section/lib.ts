export const SECTIONS_URL = "sections";

export function getSectionsHREF(id: string) {
  return `/${SECTIONS_URL}/${id}`;
}
