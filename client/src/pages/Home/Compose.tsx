import { SectionsCardSwiper } from "@/features/sections-routing";
import { HomePageLayout } from "./ui/Layout";
import { Header } from "@/widgets/Header";
import { NewsCardBar } from "@/features/news/compose/NewsCardBar";
import { FeedbackFormCard } from "@/features/feedback";
import { Section } from "@/entities/section";
import {
  Subsection,
  SubsectionCard,
  getSubsectionHref,
} from "@/entities/subsection";

export function Home({
  sections,
  random_subsections,
}: {
  sections: Section[];
  random_subsections: Subsection[];
}) {
  return (
    <HomePageLayout
      pageTitle={"Shue"}
      header={<Header sections={sections} />}
      swiper={<SectionsCardSwiper sections={sections} />}
      navbarItems={random_subsections.slice(0, 6).map(({ title, id }) => {
        return {
          id,
          element: <SubsectionCard text={title} href={getSubsectionHref(id)} />,
        };
      })}
      news={<NewsCardBar />}
      feedbackForm={<FeedbackFormCard />}
    />
  );
}
