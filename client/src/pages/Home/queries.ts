import { getAllSections } from "@/entities/section";
import { getRandomSubsections } from "@/entities/subsection/api";
import { mock_sections } from "@/mocks/section";

export const homeGetStaticProps = async () => {
  const sections = await getAllSections();
  const random_subsections = await getRandomSubsections();
  return {
    props: {
      sections: sections.data,
      random_subsections: random_subsections.data,
      // random_subsections: [],
    },
  };
  // const sections = mock_sections;
  // return {
  //   props: {
  //     sections: sections,
  //     random_subsections: [],
  //   },
  // };
};
