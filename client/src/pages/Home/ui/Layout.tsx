import { Footer } from "@/shared/ui/footer";
import { BasePageLayout } from "@/shared/ui/layouts/base-page-layout/BasePageLayout";
import {
  GridNavbarItemDecorator,
  GridNavbarLayout,
} from "@/shared/ui/layouts/grid-navbar-layout/GridNavbarLayout";
import { childElement } from "@/shared/ui/layouts/item-with-childrens-layout/ItemWithChildrenLayout";
import React from "react";

export const HomePageLayout = ({
  header,
  pageTitle,
  swiper,
  navbarItems,
  feedbackForm,
  news,
}: {
  header: React.ReactNode;
  pageTitle: string;
  swiper: React.ReactNode;
  navbarItems: childElement[];
  feedbackForm: React.ReactNode;
  news: React.ReactNode;
}) => {
  return (
    <BasePageLayout header={header} footer={<Footer />} pageTitle={pageTitle}>
      <main>
        <section>{swiper}</section>
        <section>
          <GridNavbarLayout
            navbarItems={
              navbarItems ? (
                navbarItems.map((item) => {
                  return (
                    <GridNavbarItemDecorator key={item.id}>
                      {item.element}
                    </GridNavbarItemDecorator>
                  );
                })
              ) : (
                <h2>Ошибка при загрузке информации о разделе</h2>
              )
            }
          />
          {feedbackForm}
        </section>
        <section style={{ marginBottom: "30px", marginTop: "30px" }}>
          {news}
        </section>
      </main>
    </BasePageLayout>
  );
};
