import React from "react";
import { BasePageLayout } from "@/shared/ui/layouts/base-page-layout/BasePageLayout";
import { Footer } from "@/shared/ui/footer";
import {
  ItemWithChildrensLayout,
  childElement,
} from "@/shared/ui/layouts/item-with-childrens-layout/ItemWithChildrenLayout";

export const Layout = ({
  header,
  pageTitle,
  childrenList,
  title,
  description,
}: {
  header: React.ReactNode;
  pageTitle: string;
  childrenList: childElement[];
  title: string;
  description: string;
}) => {
  return (
    <BasePageLayout header={header} footer={<Footer />} pageTitle={pageTitle}>
      <ItemWithChildrensLayout
        title={title}
        childrenList={childrenList}
        description={description}
      />
    </BasePageLayout>
  );
};
