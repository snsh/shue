export { SectionPage } from "./Compose";
export {
  sectionPageGetStaticProps,
  sectionPageGetStaticPaths,
} from "./queries";
