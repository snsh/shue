import { getAllSections, getSectionWithItemsById } from "@/entities/section";
import { Subsection } from "@/entities/subsection";
import { SectionPageProps } from "./Compose";
import type { GetStaticProps } from "next";
import { ParsedUrlQuery } from "querystring";
import { mock_sections } from "@/mocks/section";

interface Params extends ParsedUrlQuery {
  id: string;
}

export const sectionPageGetStaticProps = (async (context) => {
  const { id } = context.params as Params;
  const sections = await getAllSections();
  const section = await getSectionWithItemsById<Subsection>(id);
  return {
    props: {
      section: section.data,
      allSections: sections.data,
    },
  };
  // const sections = mock_sections;
  // const section = mock_sections.find((item) => item.id == id) || {
  //   id: "1",
  //   title: "2",
  //   description: "3",
  //   items: [],
  // };
  // return {
  //   props: {
  //     section: section,
  //     allSections: sections,
  //   },
  // };
}) satisfies GetStaticProps<SectionPageProps>;

export const sectionPageGetStaticPaths = async () => {
  const sections = await getAllSections();
  const paths = sections.data.map(({ id }) => ({
    params: {
      id: id.toString(),
    },
  }));
  return {
    paths,
    fallback: false,
  };

  // const sections = mock_sections;
  // const paths = sections.map(({ id }) => ({
  //   params: {
  //     id: id.toString(),
  //   },
  // }));

  // return {
  //   paths,
  //   fallback: false,
  // };
};
