import { Layout } from "./ui/Layout";
import { Header } from "@/widgets/Header";
import { Section, SectionWithItems } from "@/entities/section";
import {
  Subsection,
  SubsectionCard,
  getSubsectionHref,
} from "@/entities/subsection";

export type SectionPageProps = {
  section: SectionWithItems<Omit<Subsection, "description">>;
  allSections: Section[];
};
export function SectionPage({ allSections, section }: SectionPageProps) {
  return (
    <Layout
      pageTitle={`Shue | ${section.title}`}
      header={<Header sections={allSections} />}
      title={section.title}
      description={section.description}
      childrenList={section.subsections.map(({ title, id }) => {
        return {
          id: id,
          element: <SubsectionCard text={title} href={getSubsectionHref(id)} />,
        };
      })}
    />
  );
}
