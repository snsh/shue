import { getAllArticles, getArticleById } from "@/entities/article/api";
import { getAllSections, getSectionWithItemsById } from "@/entities/section";
import { Params } from "@/shared/types/query_params";
import type { GetStaticProps } from "next";
import { ArticlePageProps } from "./Compose";
import { mock_articles } from "@/mocks/article";
import { mock_sections } from "@/mocks/section";

export const articlePageGetStaticProps = (async (context) => {
  const { id } = context.params as Params;
  const sections = await getAllSections();
  const article = await getArticleById(id);
  return {
    props: {
      article: article.data,
      allSections: sections.data,
    },
  };
}) satisfies GetStaticProps<ArticlePageProps>;

//   const sections = mock_sections;
//   const article = mock_articles.find((item) => item.id == id) || {
//     id: "1",
//     content: "1",
//     description: "dd",
//     createdAt: "aaa",
//     keyWords: [],
//     title: "",
//     author: "",
//   };
//   return {
//     props: {
//       article: article,
//       allSections: sections,
//     },
//   };
// }) satisfies GetStaticProps<ArticlePageProps>;

export const articlePageGetStaticPaths = async () => {
  const articles = await getAllArticles();
  const paths = articles.data.map(({ id }) => ({
    params: {
      id: id.toString(),
    },
  }));

  return {
    paths,
    fallback: false,
  };
};

//   const articles = mock_articles;
//   const paths = articles.map(({ id }) => ({
//     params: {
//       id: id.toString(),
//     },
//   }));

//   return {
//     paths,
//     fallback: false,
//   };
// };
