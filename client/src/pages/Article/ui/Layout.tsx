import { Footer } from "@/shared/ui/footer";
import { BasePageLayout } from "@/shared/ui/layouts/base-page-layout/BasePageLayout";
import React from "react";
import styles from "./layout.module.scss";
import { style_decorator } from "@/shared/utils/css_module_decorator";
import { Parser } from "html-to-react";
import { replaceImageUrl } from "./../lib";

const gs = style_decorator(styles);

export const ArticlePageLayout = ({
  header,
  pageTitle,
  title,
  content,
}: {
  header: React.ReactNode;
  pageTitle: string;
  title: string;
  content: string;
}) => {
  return (
    <BasePageLayout header={header} footer={<Footer />} pageTitle={pageTitle}>
      <main className={gs("root")}>
        <h2 className={gs("root__tile title")}>{title}</h2>
        <div className={gs("root__content content")}>
          {Parser().parse(replaceImageUrl(content))}
        </div>
      </main>
    </BasePageLayout>
  );
};
