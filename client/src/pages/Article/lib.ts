export function replaceImageUrl(content: string) {
  const regexp = /src="content/gm;
  return content.replace(
    regexp,
    `src="${process.env.NEXT_PUBLIC_API_URL}/content`
  );
}
