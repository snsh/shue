import React from "react";
import { ArticlePageLayout } from "./ui/Layout";
import { Header } from "@/widgets/Header";
import { Article } from "@/entities/article";
import { Section } from "@/entities/section";

export type ArticlePageProps = {
  article: Article;
  allSections: Section[];
};
export function ArticlePage({ article, allSections }: ArticlePageProps) {
  return (
    <ArticlePageLayout
      header={<Header sections={allSections} />}
      pageTitle={`Shue | ${article.title}`}
      title={article.title}
      content={article.content}
    />
  );
}
