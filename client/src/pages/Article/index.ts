export { ArticlePage } from "./Compose";
export {
  articlePageGetStaticPaths,
  articlePageGetStaticProps,
} from "./queries";
