import { SignUpForm, accountStore } from "@/features/auth";
import { Layout } from "./ui/Layout";
import { LogoHeader } from "@/shared/ui/header/LogoHeader";
import { useRouter } from "next/router";
import { useAppSelector } from "@/shared/lib/redux";
import { useEffect } from "react";

export const SignUpPage = () => {
  const router = useRouter();
  const isUserAuthorized = useAppSelector(
    accountStore.selectors.getIsAuthorized
  );
  useEffect(() => {
    if (isUserAuthorized) router.push("/");
  }, [isUserAuthorized]);
  return (
    <Layout
      form={<SignUpForm />}
      pageTitle="Shue | SignUp"
      header={<LogoHeader />}
    />
  );
};
