import { Footer } from "@/shared/ui/footer";
import { BasePageLayout } from "@/shared/ui/layouts/base-page-layout/BasePageLayout";

export const Layout = ({
  form,
  header,
  pageTitle,
}: {
  form: React.ReactNode;
  header: React.ReactNode;
  pageTitle: string;
}) => {
  return (
    <BasePageLayout header={header} footer={<Footer />} pageTitle={pageTitle}>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginTop: "2rem",
          height: "100%",
        }}
      >
        {form}
      </div>
    </BasePageLayout>
  );
};
