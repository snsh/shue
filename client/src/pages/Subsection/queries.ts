import { Article } from "@/entities/article/model";
import { getAllSections } from "@/entities/section";
import { getSubsectionWithItemsById } from "@/entities/subsection";
import type { GetStaticProps } from "next";
import { Params } from "next/dist/shared/lib/router/utils/route-matcher";
import { SubsectionPageProps } from "./Compose";
import { getAllSubsections } from "@/entities/subsection/";
import { mock_subsections } from "@/mocks/subsection";
import { mock_sections } from "@/mocks/section";

export const subsectionPageGetStaticProps = (async (context) => {
  const { id } = context.params as Params;
  const sections = await getAllSections();
  const subsection = await getSubsectionWithItemsById<
    Pick<Article, "id" | "title">
  >(id);
  return {
    props: {
      subsection: subsection.data,
      allSections: sections.data,
    },
  };
}) satisfies GetStaticProps<SubsectionPageProps>;

//   const { id } = context.params as Params;
//   const sections = mock_sections;
//   const subsection = mock_subsections.find((item) => item.id == id) || {
//     id: "seed1243",
//     title: "2",
//     description: "3",
//     articles: [],
//     qandas: [],
//     supcenters: [],
//   };
//   return {
//     props: {
//       subsection: subsection,
//       allSections: sections,
//     },
//   };
// }) satisfies GetStaticProps<SubsectionPageProps>;

export const subsectionPageGetStaticPaths = async () => {
  const subsections = await getAllSubsections();

  const paths = subsections.data.map(({ id }) => ({
    params: {
      id: id.toString(),
    },
  }));

  return {
    paths,
    fallback: false,
  };
};

//   const subsections = mock_subsections;
//   const paths = subsections.map(({ id }) => ({
//     params: {
//       id: id.toString(),
//     },
//   }));
//   return {
//     paths,
//     fallback: false,
//   };
// };
