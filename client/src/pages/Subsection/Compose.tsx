import { Layout } from "./ui/Layout";
import { Header } from "@/widgets/Header";
import { Section } from "@/entities/section";
import { SubsectionWithItems } from "@/entities/subsection";
import { Article } from "@/entities/article";
import { ArticleRowCard } from "@/entities/article/ui/ArticleRowCard";
import { getArticleHref } from "@/entities/article";

export type SubsectionPageProps = {
  subsection: SubsectionWithItems<Pick<Article, "id" | "title">>;
  allSections: Section[];
};
export function SubsectionPage({
  allSections,
  subsection,
}: SubsectionPageProps) {
  const articles = subsection.articles.map(({ title, id }) => {
    return {
      id: id,
      element: <ArticleRowCard text={title} href={getArticleHref(id)} />,
    };
  });

  // const qandas = subsection.qandas.map(({ title, id }) => {
  //   return {
  //     id: id,
  //     element: <ArticleRowCard text={title} href={getArticleHref(id)} />,
  //   };
  // });

  // const supcenters = subsection.supcenters.map(({ title, id }) => {
  //   return {
  //     id: id,
  //     element: <ArticleRowCard text={title} href={getArticleHref(id)} />,
  //   };
  // });

  return (
    <Layout
      pageTitle={`Shue | ${subsection.title}`}
      header={<Header sections={allSections} />}
      title={subsection.title}
      description={subsection.description}
      childrenList={articles}
    />
  );
}
