import { accountStore } from "@/features/auth";
import { useAppDispatch, useAppSelector } from "@/shared/lib/redux";
import React, { useCallback, useEffect } from "react";

export const Loader = ({ children }: { children: React.ReactNode }) => {
  let isUserAuthorized = useAppSelector(accountStore.selectors.getIsAuthorized);
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (localStorage.getItem("token") && !isUserAuthorized) {
      dispatch(accountStore.actions.getUserData);
    }
  }, [isUserAuthorized]);
  return <>{children}</>;
};
