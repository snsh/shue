import type { AppProps } from "next/app";
import { ThemeProvider } from "@/shared/providers/ThemeProvider";
import { Inter } from "next/font/google";
import "./normalize.scss";
import "./global.scss";
import { Provider } from "react-redux";
import { store } from "@shared/lib/redux";
import { Loader } from "./loader";
import { MySnackBar } from "@/shared/modules/Snackbar";

const inter = Inter({ subsets: ["latin"] });

export function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Loader>
        <ThemeProvider>
          <div className={`${inter.className}  app-wrapper`}>
            <Component {...pageProps} />
            <MySnackBar className="snackbar" />
          </div>
        </ThemeProvider>
      </Loader>
    </Provider>
  );
}
