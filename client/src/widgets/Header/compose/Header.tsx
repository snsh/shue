import React from "react";
import { Layout } from "../ui/Layout";
import {
  SearchButton,
  SearchForm,
  SearchProvider,
} from "@/features/search-info";
import { SectionsNavbar } from "@/features/sections-routing/";
import { Logo } from "../ui/Logo";
import { SignLinks } from "../ui/SignLinks";
import { Section } from "@/entities/section";
import { useAppSelector } from "@/shared/lib/redux";
import { accountStore } from "@/features/auth";

export const Header = ({ sections }: { sections: Section[] }) => {
  let isUserAuthorized = useAppSelector(accountStore.selectors.getIsAuthorized);

  return (
    <SearchProvider>
      <Layout
        searchButton={<SearchButton />}
        navbar={<SectionsNavbar sections={sections} />}
        logo={<Logo />}
        auth={isUserAuthorized ? null : <SignLinks />}
        searchForm={<SearchForm />}
      />
    </SearchProvider>
  );
};
