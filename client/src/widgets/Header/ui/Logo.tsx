import { useRouter } from "next/router";
import React from "react";

export const Logo = () => {
  const router = useRouter();
  return (
    <div className="logo">
      <img
        src="/static/logo/logo.svg"
        alt="logo"
        onClick={() => router.push("/")}
      />
      <div className="logo__description description">
        <h3 className="description__text text">
          Информационный ресурс об ОВЗ <br /> Помощь для каждого
        </h3>
      </div>
    </div>
  );
};
