import React from "react";
import "../ui/header.scss";

export const Layout = ({
  logo,
  auth,
  navbar,
  searchButton,
  searchForm,
}: {
  logo: React.ReactNode;
  auth: React.ReactNode;
  navbar: React.ReactNode;
  searchButton: React.ReactNode;
  searchForm: React.ReactNode;
}) => {
  return (
    <header className="header">
      <div className="top-block">
        {logo}
        <div className="top-block__links links">{auth}</div>
      </div>
      <div className="header__bottom">
        {navbar}
        {searchButton}
        <div className="header__underline"></div>
      </div>
      {searchForm}
    </header>
  );
};
