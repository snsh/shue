import { SignInLink, SignUpLink } from "@/features/auth";
import React from "react";

export const SignLinks = () => {
  return (
    <>
      <div className="links__item">
        <SignInLink />
      </div>
      <div className="links__item">
        <SignUpLink />
      </div>
    </>
  );
};
