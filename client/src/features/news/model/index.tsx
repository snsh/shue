import { CardProps } from "@/shared/ui/card/Card";

export type NewsCardProps = CardProps & {
  img_src: string;
  href: string;
  title: string;
  description: string;
};
