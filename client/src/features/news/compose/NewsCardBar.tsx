import React from "react";
import {
  GridNavbarItemDecorator,
  GridNavbarLayout,
} from "@/shared/ui/layouts/grid-navbar-layout/GridNavbarLayout";
import { NewsCard } from "..";

export const NewsCardBar = () => {
  const news = [
    {
      id: "1",
      href: "#",
      title: "Академия для ухаживающих",
      description:
        "С 2015 года главным направлением в развитии проекта стало проведение Всероссийского скрининга ранних симптомов когнитивных нарушений пожилого возраста.",
      img_src:
        "https://vsluh.ru/upload/archive/novosti/sport/e7c/tyumentsy-na-skorost-soberut-kubik-rubika_358148/images/d21fdcf9581857fac4f54e31c6e3c0d0.jpg",
    },
    {
      id: "2",
      href: "#",
      title: "Академия для ухаживающи",
      description:
        "С 2015 года главным направлением в развитии проекта стало проведение Всероссийского скрининга ранних симптомов когнитивных нарушений пожилого возраста. Мы привлекли десятки врачей и бесплатно обследовали пожилых людей более чем в 70 городах России.",
      img_src:
        "https://vsluh.ru/upload/archive/novosti/sport/e7c/tyumentsy-na-skorost-soberut-kubik-rubika_358148/images/d21fdcf9581857fac4f54e31c6e3c0d0.jpg",
    },
    {
      id: "3",
      href: "#",
      title: "Академия для уаживающих",
      description:
        "С 2015 года главным направлением в развитии проекта стало проведение скрининга. Мы привлекли десятки врачей и бесплатно обследовали пожилых людей более чем в 70 городах России.",
      img_src:
        "https://vsluh.ru/upload/archive/novosti/sport/e7c/tyumentsy-na-skorost-soberut-kubik-rubika_358148/images/d21fdcf9581857fac4f54e31c6e3c0d0.jpg",
    },
  ];
  return (
    <GridNavbarLayout
      navbarItems={news.map((item) => {
        return (
          <GridNavbarItemDecorator key={item.id}>
            <NewsCard {...item} />
          </GridNavbarItemDecorator>
        );
      })}
    />
  );
};
