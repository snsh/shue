import React from "react";
import { Card } from "@/shared/ui/card/Card";
import Link from "next/link";
import { NewsCardProps } from "../../model";
import styles from "./news-card.module.scss";
import { style_decorator } from "@/shared/utils/css_module_decorator";

const gs = style_decorator(styles);

export const NewsCard = ({
  img_src,
  href,
  title,
  description,
  ...props
}: NewsCardProps) => {
  return (
    <Card className={gs("root")} {...props}>
      <div className={gs("root__wrapper wrapper")}>
        <div className={gs("root__img")}>
          <img src={img_src} alt="новость" />
        </div>
        <div className={gs("root__text text")}>
          <span className={gs("text__title title")}>{title}</span>
          <span className={gs("text__description description")}>
            {description}
          </span>
        </div>
      </div>
      <Link className={gs("root__link")} href={href} />
    </Card>
  );
};
