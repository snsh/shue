import { createUser } from "./account-slice";

export type SignUpInputs = {
  role: "Специалист" | "Посетитель";
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};

export async function onSubmit(data: SignUpInputs) {
  createUser(data);
}

export const formTextField = [
  { name: "firstName", label: "Имя", type: "text" },
  { name: "lastName", label: "Фамилия", type: "text" },
  { name: "email", label: "Email", type: "email" },
  { name: "password", label: "Пароль", type: "password" },
];
