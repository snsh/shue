import { SubmitHandler } from "react-hook-form";

export type SignInInputs = {
  role: "doctor" | "pacient";
  email: string;
  password: string;
};

export const formTextField = [
  { name: "email", label: "Email", type: "email" },
  { name: "password", label: "Пароль", type: "password" },
];

export const onSubmit: SubmitHandler<SignInInputs> = async (data) => {
  try {
  } catch (err) {
    console.log("Ошибка при отправке данных на сервер");
  }
};
