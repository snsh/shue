import { User } from "@/entities/user";
import {
  createSlice,
  createAsyncThunk,
  createSelector,
} from "@reduxjs/toolkit";
import { $api } from "@/shared/api/axios-instance";
import { SignInInputs } from "./sign-in-model";
import { SignUpInputs } from "./sign-up-model";
import { createBaseSelector, registerSlice } from "@/shared/lib/redux";
import { snackbarStore } from "@/shared/modules/Snackbar";

const initialState = {
  userData: {} as User,
  userDataLoadingStatus: "idle",
  isUserAuthorized: false,
};

export const getUserData = createAsyncThunk(
  "account/getUserData",
  async (_, { rejectWithValue }) => {
    try {
      const response = await $api.get("/get_user-data");
      return response.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const authorizeUser = createAsyncThunk(
  "account/authorizedUser",
  async (data: SignInInputs, { dispatch, rejectWithValue }) => {
    try {
      const response = await $api.post("/user_authorize", data);
      dispatch(
        snackbarStore.actions.handleAdd({
          message: "Авторизация прошла успешно",
          status: "success",
        })
      );
      return response.data;
    } catch (error: any) {
      dispatch(
        snackbarStore.actions.handleAdd({
          message: error.message,
          status: "warning",
        })
      );
      return rejectWithValue(error);
    }
  }
);

export const createUser = createAsyncThunk(
  "account/createUser",
  async (data: SignUpInputs, { dispatch, rejectWithValue }) => {
    try {
      const response = await $api.post("/user_create", data);
      dispatch(
        snackbarStore.actions.handleAdd({
          message: response.data.message,
          status: "success",
        })
      );
      return response.data;
    } catch (err: any) {
      dispatch(
        snackbarStore.actions.handleAdd({
          message: err.message,
          status: "warning",
        })
      );
      return rejectWithValue(err);
    }
  }
);

const accountSlice = createSlice({
  name: "account",
  initialState,
  reducers: {
    signOut(state, action) {
      state = initialState;
      localStorage.removeItem("token");
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getUserData.pending, (state) => {
        state.userDataLoadingStatus = "loading";
      })
      .addCase(getUserData.fulfilled, (state, action) => {
        state.userDataLoadingStatus = "idle";
        state.userData = action.payload;
      })
      .addCase(getUserData.rejected, (state, action) => {
        state.isUserAuthorized = false;
        state.userDataLoadingStatus = "error";
      })
      .addCase(authorizeUser.fulfilled, (state, action) => {
        state.isUserAuthorized = true;
        localStorage.setItem("token", action.payload.token);
      })
      // .addCase(authorizeUser.rejected, (state, action) => {
      //   state.isUserAuthorized = true;
      // })
      .addCase(createUser.pending, (state) => {
        state.userDataLoadingStatus = "loading";
      })
      .addCase(createUser.fulfilled, (state, action) => {
        state.userDataLoadingStatus = "idle";
        localStorage.setItem("token", action.payload.token);
        state.isUserAuthorized = true;
        //передать текст в всплывающее окно
      })
      .addCase(createUser.rejected, (state, action) => {
        state.isUserAuthorized = false;
        state.userDataLoadingStatus = "idle";
        //передать текст в всплывающее окно
      })
      .addDefaultCase(() => {});
  },
});

const baseSelector = createBaseSelector(accountSlice);
registerSlice([accountSlice]);

export const accountStore = {
  actions: {
    ...accountSlice.actions,
    createUser,
    getUserData,
    authorizeUser,
  },
  selectors: {
    getIsAuthorized: createSelector(
      baseSelector,
      (state) => state.isUserAuthorized
    ),
    getUserData: createSelector(baseSelector, (state) => state.userData),
    getLoadingStatus: createSelector(
      baseSelector,
      (state) => state.userDataLoadingStatus
    ),
  },
};
