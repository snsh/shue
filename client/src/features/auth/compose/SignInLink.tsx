import React, { useEffect, useState } from "react";
import { Link } from "@/shared/ui/link/Link";
import "../ui/sign-link/sign-link.scss";
import { IconForward } from "@consta/icons/IconForward";
import { useConstaSize } from "@/shared/hooks/useConstaSize";
import { window_sizes } from "@/shared/types/window_sizes";

export const SignInLink = () => {
  const [size, setSize] = useState<window_sizes | undefined>(window_sizes.S);
  const constaSize = useConstaSize();
  useEffect(() => {
    setSize(constaSize.medium);
  }, [constaSize]);

  return (
    <Link href="/sign-in" className="sign-link">
      <IconForward size={size} className="sign-link__icon" />
      Войти
    </Link>
  );
};
