import { FieldErrors, useForm } from "react-hook-form";
import { SignInInputs, formTextField } from "../model/sign-in-model";
import styles from "../ui/sign-up-form.module.scss";
import { style_decorator } from "@/shared/utils/css_module_decorator";
import { ControllInput } from "@/shared/ui/controll-input/ControlInput";
import { PrimaryButton } from "@/shared/ui/primary-button/PrimaryButton";
import { accountStore } from "..";
import { useAppDispatch, useAppSelector } from "@/shared/lib/redux";

const gs = style_decorator(styles);

export function SignInForm() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<SignInInputs>();

  const dispatch = useAppDispatch();

  const disable = useAppSelector(accountStore.selectors.getLoadingStatus);

  const onSubmit = async (data: SignInInputs) => {
    dispatch(accountStore.actions.authorizeUser(data));
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={gs("root")}>
      {formTextField.map((item) => {
        return (
          <div className={gs("root__form-item form-item")} key={item.name}>
            <ControllInput
              name={item.name}
              defaultValue=""
              control={control}
              placeholder={item.label}
              type={item.type}
              width="full"
            />
            {errors[item.name as keyof FieldErrors<SignInInputs>] && (
              <span>Ошибка ввода</span>
            )}
          </div>
        );
      })}

      <PrimaryButton
        type="submit"
        label={"Войти"}
        className={gs("root__form-item form-item")}
        disabled={disable == "loading"}
      />
    </form>
  );
}
