import React, { useEffect, useState } from "react";
import { Link } from "@/shared/ui/link/Link";
import "../ui/sign-link/sign-link.scss";
import { IconUser } from "@consta/icons/IconUser";
import { useConstaSize } from "@/shared/hooks/useConstaSize";
import { window_sizes } from "@/shared/types/window_sizes";

export const SignUpLink = () => {
  const [size, setSize] = useState<window_sizes | undefined>(window_sizes.S);
  const constaSize = useConstaSize();
  useEffect(() => {
    setSize(constaSize.medium);
  }, [constaSize]);
  return (
    <Link href="/sign-up" className="sign-link">
      <IconUser size={size} className="sign-link__icon" />
      Регистрация
    </Link>
  );
};
