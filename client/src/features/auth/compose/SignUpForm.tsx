import { useRouter } from "next/router";
import { Controller, FieldErrors, useForm } from "react-hook-form";
import { PrimaryButton } from "@/shared/ui/primary-button/PrimaryButton";
import { style_decorator } from "@/shared/utils/css_module_decorator";
import styles from "../ui/sign-up-form.module.scss";
import { ControllInput } from "@/shared/ui/controll-input/ControlInput";
import { ChoiceGroup } from "@consta/uikit/ChoiceGroup";
import { SignUpInputs, formTextField } from "../model/sign-up-model";
import { useState } from "react";
import { accountStore, createUser } from "../model/account-slice";
import { useAppDispatch, useAppSelector } from "@/shared/lib/redux";

const gs = style_decorator(styles);

export function SignUpForm() {
  const [role, setRole] = useState<"Посетитель" | "Специалист">("Посетитель");
  const dispatch = useAppDispatch();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<SignUpInputs>();

  const disable = useAppSelector(accountStore.selectors.getLoadingStatus);
  const onSubmit = async (data: SignUpInputs) => {
    dispatch(accountStore.actions.createUser(data));
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={gs("root")}>
      <Controller
        defaultValue={"Посетитель"}
        name="role"
        control={control}
        render={() => {
          return (
            <ChoiceGroup
              view="ghost"
              items={["Посетитель", "Специалист"] as const}
              getItemLabel={(item: string) => item}
              multiple={false}
              onChange={({ value }) => setRole(value)}
              name="role"
              value={role}
            />
          );
        }}
      />

      {formTextField.map((item) => {
        return (
          <div className={gs("root__form-item form-item")} key={item.name}>
            <ControllInput
              name={item.name}
              defaultValue=""
              control={control}
              placeholder={item.label}
              type={item.type}
              width="full"
            />
            {errors[item.name as keyof FieldErrors<SignUpInputs>] && (
              <span>Ошибка ввода</span>
            )}
          </div>
        );
      })}

      <PrimaryButton
        className={gs("root__form-item form-item")}
        type="submit"
        label={"Зарегистрироваться"}
        disabled={disable == "loading"}
      />
    </form>
  );
}
