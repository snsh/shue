import React from "react";
import { PrimaryButton } from "@/shared/ui/primary-button/PrimaryButton";
import { FeedbackCardLayout } from "../ui/FeedBackCardLayout";
import { FeedbackForm } from "../ui/feedback-form/FeedBackForm";
import { TextInput } from "@/shared/ui/text-input";

export const FeedbackFormCard = () => {
  return (
    <FeedbackCardLayout
      form={
        <FeedbackForm
          themeInput={
            <TextInput
              type="text"
              placeholder="Тема вопроса"
              required
              width="full"
            />
          }
          questionInput={
            <TextInput
              type="textarea"
              placeholder="Содержание"
              required
              width="full"
              rows={7}
            />
          }
          emailInput={
            <TextInput
              type="email"
              placeholder="Email для ответа"
              required
              width="full"
            />
          }
          submitButton={
            <PrimaryButton
              type="submit"
              label="Отправить вопрос"
              width="full"
            />
          }
        />
      }
    />
  );
};
