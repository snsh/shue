import React from "react";
import { Card } from "@/shared/ui/card/Card";
import { Grid, GridItem } from "@consta/uikit/Grid";
import * as breakpoints from "./breakpoints";
import styles from "./feedback-card.module.scss";
import { style_decorator } from "@/shared/utils/css_module_decorator";

const gs = style_decorator(styles);

export const FeedbackCardLayout = ({ form }: { form: React.ReactNode }) => {
  return (
    <Card className={gs("root")}>
      <Grid
        cols="4"
        gap="xl"
        className={gs("root__wrapper")}
        breakpoints={breakpoints.feedback_card}
      >
        <GridItem
          className={gs("root__info")}
          breakpoints={breakpoints.info}
          colStart="1"
          rowStart="1"
        >
          <h3 className={gs("info__title title")}>Задать вопрос</h3>
          <span className={gs("info__description description")}>
            Редакция проекта с радостью ответит на ваш вопрос. Срок
            предоставления ответа индивидуальный
          </span>
        </GridItem>
        <GridItem breakpoints={breakpoints.form}>{form}</GridItem>
      </Grid>
    </Card>
  );
};
