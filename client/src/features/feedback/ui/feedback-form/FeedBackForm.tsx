import React from "react";
import styles from "./feedback-form.module.scss";
import { style_decorator } from "@/shared/utils/css_module_decorator";

const gs = style_decorator(styles);

export const FeedbackForm = ({
  themeInput,
  questionInput,
  emailInput,
  submitButton,
}: {
  themeInput: React.ReactNode;
  questionInput: React.ReactNode;
  emailInput: React.ReactNode;
  submitButton: React.ReactNode;
}) => {
  return (
    <form action="#" className={gs("root")}>
      <div className={gs("root__wrapper")}>
        <div className={gs("root__item")}>{themeInput}</div>
        <div className={gs("root__item")}>{questionInput}</div>
        <div className={gs("root__item")}>{emailInput}</div>
        <div className={gs("root__item")}>{submitButton}</div>
      </div>
    </form>
  );
};
