import React from "react";
import { SectionsNavbarLayout } from "../ui/section-links-navbar/SectionsNavbarLayout";
import { SectionsNavbarList } from "../ui/section-links-navbar/SectionsNavbarList";
import { SECTIONS_URL, Section } from "@/entities/section";

type SectionLinkProps = { label: string; href: string };

export const SectionsNavbar = ({
  sections,
  className,
}: {
  sections: Section[];
  className?: string;
}) => {
  const linksInfo: SectionLinkProps[] = sections.map((section) => {
    return { label: section["title"], href: `/${SECTIONS_URL}/${section.id}` };
  });
  return (
    <SectionsNavbarLayout
      className={className}
      sectionList={<SectionsNavbarList links={linksInfo} />}
    />
  );
};
