import { SwiperSlide } from "swiper/react";
import { Swiperlayout } from "../ui/section-cards-swiper/SwiperLayout";
import { SectionCard } from "../ui/section-cards-swiper/section-card/SectionCard";
import { SectionLinkButton } from "../ui/section-cards-swiper/section-link-button/SectionLinkButton";
import { Section } from "@/entities/section";
import { getSectionsHREF } from "@/entities/section";

export const SectionsCardSwiper = ({ sections }: { sections: Section[] }) => {
  return (
    <Swiperlayout
      slidesList={sections.map(({ ...props }) => {
        return (
          <SwiperSlide key={props.id}>
            <SectionCard
              {...props}
              actions={<SectionLinkButton href={getSectionsHREF(props.id)} />}
            />
          </SwiperSlide>
        );
      })}
    />
  );
};
