import { Section } from "@/entities/section";
import { CardProps } from "@/shared/ui/card/Card";
import { PrimaryButtonProps } from "@/shared/ui/primary-button/PrimaryButton";

export type SectionCardEntity = Section & CardProps;

export type SectionLinkButtonProps = PrimaryButtonProps & { href: string };
