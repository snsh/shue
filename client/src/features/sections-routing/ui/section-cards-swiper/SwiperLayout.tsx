import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/effect-fade";
import { Swiper } from "swiper/react";
import { Navigation, EffectFade, Autoplay } from "swiper/modules";
import "./card-swiper.scss";

export const Swiperlayout = ({
  slidesList,
}: {
  slidesList: React.ReactNode;
}) => {
  return (
    <div className="swiper__wrapper">
      <Swiper
        autoplay={{
          delay: 70000,
          disableOnInteraction: false,
        }}
        loop={true}
        effect={"fade"}
        navigation={true}
        modules={[Navigation, EffectFade, Autoplay]}
        spaceBetween={50}
        slidesPerView={1}
        className="swiper"
      >
        {slidesList}
      </Swiper>
    </div>
  );
};
