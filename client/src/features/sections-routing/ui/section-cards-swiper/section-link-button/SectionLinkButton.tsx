import React, { useEffect, useState } from "react";
import { PrimaryButton } from "@/shared/ui/primary-button/PrimaryButton";
import Link from "next/link";
import { useConstaSize } from "@/shared/hooks/useConstaSize";
import { SectionLinkButtonProps } from "@/features/sections-routing/model";
import { style_decorator } from "@/shared/utils/css_module_decorator";
import style from "./section-link-button.module.scss";
import { window_sizes } from "@/shared/types/window_sizes";

const gs = style_decorator(style);

export const SectionLinkButton = ({
  href,
  ...props
}: SectionLinkButtonProps) => {
  const [size, setSize] = useState<window_sizes | undefined>(window_sizes.L);
  const constaSize = useConstaSize();

  useEffect(() => {
    setSize(constaSize.big);
  }, [constaSize]);

  return (
    <div className={gs("root__wrapper")}>
      <Link href={href} className={gs("root__link")} />
      <PrimaryButton
        size={size}
        label="Узнать больше"
        className={gs("root__button")}
        {...props}
      ></PrimaryButton>
    </div>
  );
};
