import React from "react";
import styles from "./section-card.module.scss";
import { Card } from "@/shared/ui/card/Card";
import { check_classname } from "@/shared/utils/check_classname";
import { SectionCardEntity } from "@/features/sections-routing/model";
import { style_decorator } from "@/shared/utils/css_module_decorator";
import { apiUrl } from "@/shared/api/axios-instance";

const gs = style_decorator(styles);

export const SectionCard = ({
  title,
  description,
  image,
  actions,
  ...props
}: SectionCardEntity & { actions: React.ReactNode }) => {
  return (
    <Card className={check_classname(gs("root"), props.className)} {...props}>
      <div className={gs("root__wrapper wrapper")}>
        <div className={gs("root__left_block left_block")}>
          <h2 className={gs("root__title title")}>{title}</h2>
          <p className={gs("root__description description")}>{description}</p>
          <div className={gs("root__actions_block actions_block")}>
            {actions}
          </div>
        </div>
        <div className={gs("root__right_block right_block")}>
          <img
            src={process.env.NEXT_PUBLIC_API_URL + "/" + image}
            alt={title}
          />
        </div>
      </div>
    </Card>
  );
};
