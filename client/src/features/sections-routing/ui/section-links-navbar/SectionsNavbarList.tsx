import { Link } from "@/shared/ui/link/Link";
import React from "react";

export const SectionsNavbarList = ({
  links,
}: {
  links: { label: string; href: string }[];
}) => {
  return (
    <ul className="sections-navbar__list list">
      {links.map(({ label, href }) => {
        return (
          <li key={label} className="list__item list-item">
            <Link key={href} href={href}>
              {label}
            </Link>
          </li>
        );
      })}
    </ul>
  );
};
