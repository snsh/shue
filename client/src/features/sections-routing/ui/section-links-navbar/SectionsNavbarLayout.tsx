import { check_classname } from "@/shared/utils/check_classname";
import React from "react";
import "./sections-navbar.scss";

export const SectionsNavbarLayout = ({
  sectionList,
  className,
}: {
  sectionList: React.ReactNode;
  className?: string | null;
}) => {
  return (
    <nav className={check_classname(className, "sections-navbar")}>
      {sectionList}
    </nav>
  );
};
