import React from "react";
import "./search-form.scss";
import { check_classname } from "@/shared/utils/check_classname";

export const SearchFormLayout = ({
  textField,
  searchButton,
  cancelButton,
  className,
}: {
  textField: React.ReactNode;
  searchButton: React.ReactNode;
  cancelButton: React.ReactNode;
  className: string;
}) => {
  return (
    <div className={check_classname(className, "search-form__wrapper")}>
      <form className="search-form">
        {textField}
        {searchButton}
        {cancelButton}
      </form>
    </div>
  );
};
