import React from "react";
import { useSearch } from "../model";
import { IconClose } from "@consta/icons/IconClose";

export const CancelButton = ({ ...props }) => {
  return (
    <div className="search-form__cancel-button cancel-button">
      <button {...props}>
        <IconClose size={"s"} />
      </button>
    </div>
  );
};
