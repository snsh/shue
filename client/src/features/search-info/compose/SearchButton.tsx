import React, { useEffect, useState } from "react";
import { IconSearchStroked } from "@consta/icons/IconSearchStroked";
import { PrimaryButton } from "@/shared/ui/primary-button/PrimaryButton";
import { SearchButtonProps, useSearch } from "../model";
import { useConstaSize } from "@/shared/hooks/useConstaSize";
import { window_sizes } from "@/shared/types/window_sizes";

export const SearchButton = ({ ...props }: SearchButtonProps) => {
  const { isVisible, setVisible } = useSearch();
  const [size, setSize] = useState<window_sizes | undefined>(window_sizes.S);
  const constaSize = useConstaSize();

  useEffect(() => {
    setSize(constaSize.small);
  }, [constaSize]);
  return (
    <PrimaryButton
      size={size}
      onClick={setVisible}
      disabled={isVisible}
      label="Поиск по сайту"
      iconLeft={IconSearchStroked}
      {...props}
    />
  );
};
