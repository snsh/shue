import { ReactNode } from "react";
import { SearchFormProvider } from "../model";

export const SearchProvider = ({ children }: { children: ReactNode }) => {
  return <SearchFormProvider>{children}</SearchFormProvider>;
};
