import React from "react";
import { SearchFormLayout } from "../ui/SearchFormLayout";
import { TextInput } from "@/shared/ui/text-input";
import { PrimaryButton } from "@/shared/ui/primary-button/PrimaryButton";
import { CancelButton } from "../ui/CancelButton";
import { useSearch } from "../model";

export const SearchForm = () => {
  const { query, setQuery, isVisible, setVisible } = useSearch();
  return (
    <SearchFormLayout
      className={isVisible ? "" : "hidden"}
      textField={
        <TextInput
          value={query}
          onChange={({ value }) => setQuery(value)}
          width="full"
          type="text"
          className="search-form__text-field"
          placeholder="Введите запрос поиска"
        />
      }
      searchButton={
        <PrimaryButton
          type="submit"
          label="Найти"
          className="search-form__submit-button"
        />
      }
      cancelButton={
        <CancelButton
          onClick={(e: React.MouseEvent<HTMLElement>) => {
            e.preventDefault();
            setVisible();
          }}
        />
      }
    />
  );
};
