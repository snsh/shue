import { PrimaryButtonProps } from "@/shared/ui/primary-button/PrimaryButton";
import {
  Dispatch,
  ReactNode,
  SetStateAction,
  createContext,
  useContext,
  useMemo,
  useState,
} from "react";

export type SearchButtonProps = PrimaryButtonProps;

const searchContext = createContext<{
  query: string | null;
  setQuery: Dispatch<SetStateAction<string | null>>;
  isVisible: boolean;
  setVisible: Dispatch<SetStateAction<boolean>>;
} | null>(null);

export function SearchFormProvider({ children }: { children: ReactNode }) {
  const [query, setQuery] = useState<string | null>(null);
  const [isVisible, setVisible] = useState<boolean>(false);
  return (
    <searchContext.Provider
      value={useMemo(
        () => ({ query, setQuery, isVisible, setVisible }),
        [query, isVisible]
      )}
    >
      {children}
    </searchContext.Provider>
  );
}

export function useSearch() {
  const contextValue = useContext(searchContext);
  if (!contextValue) throw new Error("context not provided");

  const { query, setQuery, isVisible, setVisible } = contextValue;

  return {
    query,
    setQuery,
    isVisible,
    setVisible: () => {
      setVisible((prev) => !prev);
    },
  };
}
