# Description

 This project is the final qualifying work of students of St. Petersburg State University Faculty of Applied Mathematics of Control Processes Putilov A.E. and Pukaev A.E. The work is a web portal for posting information and work of specialists in the field of children with speech disabilities

# Running the server-part of app 
 go to the catalog with ```docker-compose.yml```

```
 cd .\docker-config\
 cd .\server-config\ 

 ```
 build and run docker-containers

 ```
docker-compose up --build
 ```
  after launching the application, you can go to the backend api - http://localhost:5000/api/docs#/

