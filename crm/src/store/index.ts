import { configureStore } from "@reduxjs/toolkit";
import { contentReducer } from "./content/content.slice";

export const store = configureStore({
  reducer: {
    content: contentReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
