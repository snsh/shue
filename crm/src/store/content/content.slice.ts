import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { ContentMove, ContentType } from "./model";

type ActiveItemId = string;

interface ContentState {
  type: ContentType;
  move: ContentMove;
  activeItem: ActiveItemId;
}

const initialState: ContentState = {
  type: ContentType.SECTIONS,
  move: ContentMove.GET,
  activeItem: "",
};

export const contentSlice = createSlice({
  name: "content",
  initialState,
  reducers: {
    changeContentType(state, action: PayloadAction<ContentType>) {
      state.type = action.payload;
    },
    changeContentMove(state, action: PayloadAction<ContentMove>) {
      state.move = action.payload;
    },
    changeActiveItem(state, action: PayloadAction<string>) {
      state.activeItem = action.payload;
    },
  },
});

export const contentActions = contentSlice.actions;
export const contentReducer = contentSlice.reducer;
