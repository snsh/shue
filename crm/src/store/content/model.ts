export enum ContentType {
  SECTIONS = "Разделы",
  SUBSECTIONS = "Подразделы",
  ARTICLES = "Статьи",
}

export enum ContentMove {
  GET = "Посмотреть",
  POST = "Создать",
}
