import React from "react";
import { ContentList } from "../modules/content-list";
import { Header } from "../modules/header";
import { useAppSelector } from "../hooks/redux";
import { ContentMove } from "../store/content/model";
import { PostForm } from "../modules/post-form";

export const MainPage = () => {
  const moveType = useAppSelector((state) => state.content.move);
  return (
    <div className="app-container">
      <div className="container">
        <Header />
        <div className="hr"></div>
        {moveType == ContentMove.GET && <ContentList />}
        {moveType == ContentMove.POST && <PostForm />}
      </div>
    </div>
  );
};
