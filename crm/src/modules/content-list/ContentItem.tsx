import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../hooks/redux";
import { ContentType } from "../../store/content/model";
import { SectionService } from "../../services/SectionService";
import { SubsectionService } from "../../services/SubsectionService";
import { ArticleService } from "../../services/ArticleService";
import { Button } from "@consta/uikit/Button";
import { contentString } from "./lib";

export const ContentItem = () => {
  const [activeId, type] = useAppSelector((state) => {
    return [state.content.activeItem, state.content.type];
  });
  const [data, setData] = useState<Object | null>(null);

  const handleClick = async () => {
    switch (type) {
      case ContentType.SECTIONS:
        await SectionService.deleteSection(activeId);
        break;
      case ContentType.SUBSECTIONS:
        await SubsectionService.deleteSubsection(activeId);
        break;
      case ContentType.ARTICLES:
        await ArticleService.deleteArticle(activeId);
        break;
    }
  };

  useEffect(() => {
    if (activeId) {
      switch (type) {
        case ContentType.SECTIONS:
          SectionService.getSectionById(activeId).then((data) => {
            setData(data);
          });
          break;
        case ContentType.SUBSECTIONS:
          SubsectionService.getSubsectionById(activeId).then((data) => {
            setData(data);
          });
          break;
        case ContentType.ARTICLES:
          ArticleService.getArticleById(activeId).then((data) => {
            setData(data);
          });
          break;
      }
    }
  }, [type, activeId]);

  if (!data) {
    return null;
  }

  return (
    <div className="content-item">
      {data &&
        Object.keys(data).map((key) => {
          return (
            <div className="item-field">
              <h3>{`${key}`}</h3>{" "}
              {` ${contentString(data[key as keyof typeof data])}`}
            </div>
          );
        })}
      <Button
        style={{ marginTop: "10px" }}
        label={"Удалить"}
        onClick={handleClick}
        width="full"
      />
    </div>
  );
};
