import React from "react";
import "./ui.scss";
import { useActions } from "../../hooks/actions";

export const ContentCard = ({ id, title }: { id: string; title: string }) => {
  const { changeActiveItem } = useActions();
  return (
    <div
      className="content-card-wrapper"
      onClick={() => {
        changeActiveItem(id);
      }}
    >
      <div className="card-title">{title}</div>
      <div className="card-id">id : {id}</div>
    </div>
  );
};
