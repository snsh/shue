export function contentString(content: any) {
  if (Array.isArray(content)) {
    return content.map((item) => item.title).join(", ");
  }
  return content;
}
