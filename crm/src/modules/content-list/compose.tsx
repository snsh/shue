import "./ui.scss";
import { ContentCard } from "./ContentCard";
import { useAppSelector } from "../../hooks/redux";
import { useQuery } from "@tanstack/react-query";
import { ContentType } from "../../store/content/model";
import { ContentItem } from "./ContentItem";
import { SectionService } from "../../services/SectionService";
import { SubsectionService } from "../../services/SubsectionService";
import { ArticleService } from "../../services/ArticleService";

type ListContentItem = {
  id: string;
  title: string;
};
export const ContentList = () => {
  const contentType = useAppSelector((state) => state.content.type);

  const {
    data: sections,
    isSuccess: isSectionsSucces,
    isLoading,
  } = useQuery({
    queryFn: () => SectionService.getAllSections(),
    queryKey: ["sections"],
  });
  const { data: subsections, isSuccess: isSubsectionsSuccess } = useQuery({
    queryFn: () => SubsectionService.getAllSubsections(),
    queryKey: ["subsections"],
  });
  const { data: articles, isSuccess: isArticlesSuccess } = useQuery({
    queryFn: () => ArticleService.getAllArticles(),
    queryKey: ["articles"],
  });
  let content = {
    [ContentType.SECTIONS]: sections,
    [ContentType.SUBSECTIONS]: subsections,
    [ContentType.ARTICLES]: articles,
  };

  return (
    <main className="content">
      <div className="content-list">
        {content[contentType] ? (
          content[contentType].map((item: ListContentItem) => {
            return <ContentCard {...item} />;
          })
        ) : (
          <h2>Загрузка...</h2>
        )}
      </div>
      <div className="active-item">
        <ContentItem />
      </div>
    </main>
  );
};
