import { ContentType } from "../../store/content/model";

export const SectionFormDataType = {
  title: { name: "Название", type: "text" },
  description: { name: "Описание", type: "textarea" },
  image: { name: "Изображение", type: "file-img" },
};

export const SubsectionFormDataType = {
  title: { name: "Название", type: "text" },
  description: { name: "Описание", type: "textarea" },
  parentSection: { name: "Раздел", type: "parent" },
};

export const ArticleFormDataType = {
  title: { name: "Название", type: "text" },
  author: { name: "Автор", type: "text" },
  keyWords: { name: "Ключевые слова", type: "text" },
  subsection: { name: "Подраздел", type: "parent" },
  content: { name: "Содержание", type: "file-docx" },
};

export const FormType = {
  [ContentType.SECTIONS]: SectionFormDataType,
  [ContentType.SUBSECTIONS]: SubsectionFormDataType,
  [ContentType.ARTICLES]: ArticleFormDataType,
};
