import React, { useState } from "react";
import "./ui.scss";
import { useAppSelector } from "../../hooks/redux";
import { FormType } from "./formType";
import { TextField } from "@consta/uikit/TextField";
import { Button } from "@consta/uikit/Button";
import { ContentType } from "../../store/content/model";
import { Select } from "@consta/uikit/Select";
import { useQuery } from "@tanstack/react-query";
import { SectionService } from "../../services/SectionService";
import { SubsectionService } from "../../services/SubsectionService";
import { ArticleService } from "../../services/ArticleService";

export const PostForm = () => {
  const {
    data: sections,
    isSuccess: isSectionsSucces,
    isLoading,
  } = useQuery({
    queryFn: () => SectionService.getAllSections(),
    queryKey: ["sections"],
  });
  const { data: subsections, isSuccess: isSubsectionsSuccess } = useQuery({
    queryFn: () => SubsectionService.getAllSubsections(),
    queryKey: ["subsections"],
  });

  const type = useAppSelector((state) => {
    return state.content.type;
  });
  const formType = FormType[type];
  const formTypeField = Object.keys(FormType[type]);
  const [value, setValue] = useState<Record<string, any>>(
    Object.fromEntries(formTypeField.map((item) => [item, null]))
  );

  let selectItems = type == ContentType.ARTICLES ? subsections : sections;
  selectItems = selectItems.map((item: any) => ({
    label: item.title,
    id: item.id,
  }));

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    let formData = new FormData();
    Object.keys(value).forEach((field) => {
      formData.append(
        field,
        value[field]["id"] ? value[field]["id"] : value[field]
      );
    });
    switch (type) {
      case ContentType.ARTICLES:
        await ArticleService.postArticle(formData);
        break;
      case ContentType.SECTIONS:
        await SectionService.postSection(formData);
        break;
      case ContentType.SUBSECTIONS:
        await SubsectionService.postSubsection(formData);
        break;
    }
  };

  let fieldType;
  return (
    <div className="form-wrapper">
      <h2>Заполните поля</h2>
      <form className="form-root" onSubmit={(e) => handleSubmit(e)}>
        {formTypeField.map((field) => {
          switch ((fieldType = formType[field as keyof typeof formType].type)) {
            case "text":
              return (
                <div className="form-item">
                  <TextField
                    value={value[field]}
                    onChange={(e) =>
                      setValue((prev) => ({
                        ...prev,
                        [field]: e.value ? e.value : "",
                      }))
                    }
                    type={"text"}
                    placeholder={formType[field as keyof typeof formType].name}
                    name={field}
                    width="full"
                    required
                  />
                </div>
              );
            case "file-docx":
            case "file-img":
              return (
                <div className="form-item">
                  <p>{formType[field as keyof typeof formType].name}</p>
                  <input
                    type="file"
                    accept={
                      fieldType == "file-docx" ? ".docx" : ".png,.jpg,.jpeg"
                    }
                    onChange={(e) =>
                      setValue((prev) => ({
                        ...prev,
                        [field]: e.target?.files ? e.target?.files[0] : "",
                      }))
                    }
                  />
                </div>
              );
            case "textarea":
              return (
                <div className="form-item">
                  <TextField
                    value={value[field]}
                    onChange={(e) =>
                      setValue((prev) => ({
                        ...prev,
                        [field]: e.value ? e.value : "",
                      }))
                    }
                    type={"textarea"}
                    width="full"
                    placeholder={formType[field as keyof typeof formType].name}
                    name={field}
                    required
                  />
                </div>
              );
            case "parent":
              return (
                <div className="form-item">
                  <Select
                    placeholder={formType[field as keyof typeof formType].name}
                    items={selectItems}
                    value={value[field]}
                    onChange={({ value }) => {
                      console.log(value);
                      setValue((prev) => ({
                        ...prev,
                        [field]: value ? value : {},
                      }));
                    }}
                  />
                </div>
              );
          }
        })}
        <Button
          width="default"
          className="form-submit-button"
          type="submit"
          label={"Сохранить"}
        />
      </form>
    </div>
  );
};
