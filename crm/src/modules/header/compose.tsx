import React from "react";
import { useAppSelector } from "../../hooks/redux";
import { useActions } from "../../hooks/actions";
import { ContentMove, ContentType } from "../../store/content/model";
import { ChoiceGroup } from "@consta/uikit/ChoiceGroup";

export const Header = () => {
  const moveType = useAppSelector((state) => state.content.move);
  const contentType = useAppSelector((state) => state.content.type);
  const { changeContentMove, changeContentType } = useActions();
  return (
    <header className="header">
      <ChoiceGroup
        value={contentType}
        onChange={({ value }) => changeContentType(value)}
        items={Object.values(ContentType)}
        getItemLabel={(item: any) => item}
        multiple={false}
        name="SetContentType"
      />
      <ChoiceGroup
        value={moveType}
        onChange={({ value }) => changeContentMove(value)}
        items={Object.values(ContentMove)}
        getItemLabel={(item: string) => item}
        multiple={false}
        name="SetMoveType"
      />
    </header>
  );
};
