import { useDispatch } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit";
import { contentActions } from "../store/content/content.slice";

const actions = {
  ...contentActions,
};
export const useActions = () => {
  const dispatch = useDispatch();
  return bindActionCreators(actions, dispatch);
};
