import $api from "../api/axios.instance";

export class SectionService {
  static async getAllSections() {
    try {
      const response = await $api.get("/sections");
      return response.data;
    } catch (err) {
      throw err;
    }
  }

  static async getSectionById(id: string) {
    try {
      const response = await $api.get("/sections/" + id);
      return response.data;
    } catch (err) {
      throw err;
    }
  }

  static async postSection(data: FormData) {
    try {
      const response = await $api.post("/sections", data);
      return response.data;
    } catch (err) {
      throw err;
    }
  }

  static async deleteSection(id: string) {
    try {
      const response = await $api.delete("/sections/" + id);
      return response.data;
    } catch (err) {
      throw err;
    }
  }
}
