import $api from "../api/axios.instance";

export class ArticleService {
  static async getAllArticles() {
    try {
      const response = await $api.get("/articles");
      return response.data;
    } catch (err) {
      throw err;
    }
  }

  static async getArticleById(id: string) {
    try {
      const response = await $api.get("/articles/" + id);
      return response.data;
    } catch (err) {
      throw err;
    }
  }

  static async postArticle(data: FormData) {
    try {
      const response = await $api.post("/articles", data);
      return response.data;
    } catch (err) {
      throw err;
    }
  }

  static async deleteArticle(id: string) {
    try {
      const response = await $api.delete("/articles/" + id);
      return response.data;
    } catch (err) {
      throw err;
    }
  }
}
