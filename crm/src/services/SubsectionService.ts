import $api from "../api/axios.instance";

export class SubsectionService {
  static async getAllSubsections() {
    try {
      const response = await $api.get("/subsections");
      return response.data;
    } catch (err) {
      throw err;
    }
  }

  static async getSubsectionById(id: string) {
    try {
      const response = await $api.get("/subsections/" + id);
      return response.data;
    } catch (err) {
      throw err;
    }
  }

  static async postSubsection(data: FormData) {
    try {
      const response = await $api.post("/subsections", data);
      return response.data;
    } catch (err) {
      throw err;
    }
  }

  static async deleteSubsection(id: string) {
    try {
      const response = await $api.delete("/subsection/" + id);
      return response.data;
    } catch (err) {
      throw err;
    }
  }
}
