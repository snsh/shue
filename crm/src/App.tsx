import "./app.scss";
import { Theme, presetGpnDefault } from "@consta/uikit/Theme";
import { MainPage } from "./pages/MainPage";

function App() {
  return (
    <Theme preset={presetGpnDefault}>
      <MainPage />
    </Theme>
  );
}

export default App;
