import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import * as path from 'path';
import * as fs from 'fs';
import * as uuid from 'uuid';
import {convertToHtml} from 'mammoth';
import * as base64To8UintArray from 'base64-to-uint8array';
export enum FileType{
    IMAGE = 'image',
    CONTENT = 'content'
}


@Injectable()
export class FileService {
    
    async create(type: FileType, file): Promise<string>{
        try {
            if (type == FileType.CONTENT) {
                
                var outputHtml: string;

                await convertToHtml(file.buffer)
                .then(function(result){
                    var html = result.value; // The generated HTML
                    var messages = result.messages;
                    outputHtml = html;
                })
                .catch(function(error) {
                    console.error(error);
                });
                const regexp = /src="data:image\S*"/gm;
                const matchSrc = [...outputHtml.matchAll(regexp)]
                matchSrc.forEach((img)=>{
                  	const typeAndBuffer = img[0].replace("src=\"data:image/", '').split(";");
                    const fileExtension = typeAndBuffer[0];
                  	const fileBuffer = typeAndBuffer[1].replace("base64,", '').slice(0,-1);
                    const fileName = uuid.v4() + '.' + fileExtension
                    const filePath = path.resolve(__dirname, '..', 'static', type)
                    if(!fs.existsSync(filePath)) {
                        fs.mkdirSync(filePath, {recursive: true})
                    }
                    fs.writeFileSync(path.resolve(filePath, fileName), base64To8UintArray(fileBuffer))
                    outputHtml = outputHtml.replace(/data:image\S*"/m, type + '/' + fileName+"\"");
                    console.log(type + '/' + fileName+"\""); // type + '/' + fileName
                })
                return Promise.resolve(outputHtml);
            }else{
                const fileExtension = file.originalname.split('.').pop()
                const fileName = uuid.v4() + '.' + fileExtension
                const filePath = path.resolve(__dirname, '..', 'static', type)
                console.log(filePath);
                if(!fs.existsSync(filePath)) {
                    fs.mkdirSync(filePath, {recursive: true})
                }
                fs.writeFileSync(path.resolve(filePath, fileName), file.buffer)
                return Promise.resolve(type + '/' + fileName);
            }
            
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    removeFile(){

    }

}
