import { Body, Controller, Delete, Get, Param, Post, UploadedFiles, UseInterceptors } from "@nestjs/common";
import { SectionService } from "./section.service";
import { CreateSectionDto } from "./dto/create-section.dto";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger/dist";
import { Section } from "./entities/section.entity";
import { FileFieldsInterceptor } from "@nestjs/platform-express/multer";

@ApiTags('section')
@Controller('/sections')
export class SectionController{
    constructor(
        private _sectionService: SectionService,
    ){}

    @ApiOperation({summary: 'Создание раздела'})
    @ApiResponse({status: 200, type: Section})
    @Post()
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'image', maxCount: 1 },
      ]))
    create(@UploadedFiles() files: {image?: Express.Multer.File[]}, @Body() dto: CreateSectionDto, id: string){
        return this._sectionService.create(dto,  files.image[0]);
    }

    @ApiOperation({summary: 'Получение всех разделов'})
    @ApiResponse({status: 200, type: [Section]})
    @Get()
    getAll(){
        return this._sectionService.getAll();
    }

    @ApiOperation({summary: 'Получение одного раздела по id'})
    @ApiResponse({status: 200, type: Section})
    @Get(':id')
    getOne(@Param('id') id: string){
        return this._sectionService.getOne(id);
    }

    @ApiOperation({summary: 'Удаление одного раздела по id'})
    @ApiResponse({status: 200})
    @Delete(':id')
    delete(@Param('id') id: string){
        return this._sectionService.delete(id);
    }
}