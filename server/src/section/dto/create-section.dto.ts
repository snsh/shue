import { ApiProperty } from "@nestjs/swagger/dist";

export class CreateSectionDto{
    
    @ApiProperty({example: 'Альтернативная коммуникация ', description: 'Название раздела'})
    title: string;

    @ApiProperty({example: 'Всё про альтернативную коммуникацию', description: 'Описание раздела'})
    description: string;

}