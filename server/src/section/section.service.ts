import { BadRequestException, Injectable } from '@nestjs/common';
import { Section } from './entities/section.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSectionDto } from './dto/create-section.dto';
import { FileService, FileType } from 'src/file/file.service';

@Injectable()
export class SectionService {
  constructor(
    @InjectRepository(Section) private _sectionRepository: Repository<Section>,
    private _fileService: FileService,
  ) {}

  async create(dto: CreateSectionDto, image): Promise<Section> {
    const imagePath = await this._fileService.create(FileType.IMAGE, image);
    const newSection = {
      ...dto,
      image: imagePath,
    };
    if (!newSection) throw new BadRequestException('Something went wrong...');
    else return await this._sectionRepository.save(newSection);
  }

  async getAll(): Promise<Section[]> {
    return await this._sectionRepository.find();
  }

  async getOne(id: string): Promise<Section> {
    return await this._sectionRepository.findOne({
      select:{
        id: true,
        title: true,
        description: true,
        subsections:{
          title: true,
          id: true,
        }
      },
      where: { id },
      relations: {
        subsections: true,
      },
    });
  }

  async delete(id: string): Promise<void> {
    const section = await this._sectionRepository.delete(id);
  }
}
