import { ApiProperty } from "@nestjs/swagger/dist";
import { Subsection } from "src/subsection/entities/subsection.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class Section{

    @ApiProperty({example: 'xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx', description: 'Уникальный идентификатор'})
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ApiProperty({example: 'Альтернативная коммуникация ', description: 'Название раздела'})
    @Column()
    title: string;

    @ApiProperty({example: 'image/xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx.jpg', description: 'Превью раздела'})
    @Column()
    image: string;

    @ApiProperty({example: 'Всё про альтернативную коммуникацию', description: 'Описание раздела'})
    @Column({default: ''})
    description: string;

    @OneToMany(()=>Subsection, (subsection)=> subsection.parentSection, { onDelete: 'CASCADE'})
    subsections: Subsection[];

}