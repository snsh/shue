import { Module } from '@nestjs/common';
import { ArticleModule } from './article/article.module';
import { FileModule } from './file/file.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import * as path from 'path';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfigAsync } from './config/typeorm.config';
import { UserModule } from './user/user.module';
import { SectionModule } from './section/section.module';
import { SubsectionModule } from './subsection/subsection.module';


@Module({
    imports:[
        ConfigModule.forRoot({
            envFilePath: `.${process.env.NODE_ENV}.env`
        }),
        TypeOrmModule.forRootAsync(typeOrmConfigAsync),
        ServeStaticModule.forRoot({rootPath: path.resolve(__dirname, 'static'),}),
        ArticleModule,
        SectionModule,
        SubsectionModule,
        UserModule,
        FileModule,
    ]
})
export class AppModule {}