import { ArrayOverlap, DataSource } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv';

 
config({
    path: `.${process.env.NODE_ENV}.env`
});
 
const configService = new ConfigService();
console.log(configService.get('POSTGRES_DB'));
console.log(process.env.NODE_ENV);
const dataSource = new DataSource({
    type: 'postgres',
    host: configService.get('POSTGRES_HOST') || 'localhost',
    port: configService.get('POSTGRES_PORT') || 5432,
    username: configService.get('POSTGRES_USER'),
    password: configService.get<string>('POSTGRES_PASSWORD'),
    database: configService.get('POSTGRES_DB'),
    entities: [configService.get('TYPEORM_ENTITIES')], 
    migrations: [configService.get('TYPEORM_MIGRATIONS')],
    synchronize: configService.get('TYPEORM_SYNCHRONIZE'),
});
 
dataSource.initialize()
    .then(() => {
        console.log("Data Source has been initialized!")
    })
    .catch((err) => {
        console.error("Error during Data Source initialization", err)
    })

export default dataSource;