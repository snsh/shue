import { ApiProperty } from "@nestjs/swagger/dist";
import { Article } from "src/article/entities/article.entity";
import { Section } from "src/section/entities/section.entity";

export class CreateSubsectionDto{
    
    @ApiProperty({example: 'Виды коммуникационных средств', description: 'Название подраздела'})
    title: string;
    
    @ApiProperty({example: 'xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx', description: 'Раздел, содержащий данный подраздел'})
    parentSection: Section;

    @ApiProperty({example: 'Всё про коммуникационные средства', description: 'Описание подраздела'})
    description: string;
} 