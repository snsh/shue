import { BadRequestException, Injectable } from "@nestjs/common";
import { Subsection } from "./entities/subsection.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateSubsectionDto } from "./dto/create-subsection.dto";



@Injectable()
export class SubsectionService{
    constructor(@InjectRepository(Subsection) private _subsectionRepository: Repository<Subsection>,
    ){}

    async create(dto: CreateSubsectionDto): Promise<Subsection>{
        const newSubsection = {
            ...dto,
        }
        if(!newSubsection) throw new BadRequestException('Something went wrong...')
        else return await this._subsectionRepository.save(newSubsection);
    }

    async getAll():Promise<Subsection[]>{
        return await this._subsectionRepository.find({
            select: {
                id: true,
                title: true,
            }
        })
    }

    async getOne(id: string): Promise<Subsection>{
        return await this._subsectionRepository.findOne({
            select:{
                id: true,
                title: true,
                description: true,
                articles:{
                    id: true,
                    title: true,
                }
            },
            where: {id},
            relations:{
                articles: true
            }
        });
    }

    async getRandom(): Promise<Subsection[]> {
        console.log("object");
        return await this._subsectionRepository
          .createQueryBuilder("subsection")
          .where("random() > 0.5")
          .limit(6)
          .getMany()
      }

    async delete(id: string): Promise<void>{
        const section =  await this._subsectionRepository.delete(id);
    }
    
}