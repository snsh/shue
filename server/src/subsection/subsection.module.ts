import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Subsection } from "./entities/subsection.entity";
import { SubsectionController } from "./subsection.controller";
import { SubsectionService } from "./subsection.service";
import { RandSubsectionController } from "./randsubsection.controller";



@Module({
    imports: [TypeOrmModule.forFeature([Subsection])],
    controllers: [SubsectionController, RandSubsectionController],
    providers: [SubsectionService],
})
export class SubsectionModule{}