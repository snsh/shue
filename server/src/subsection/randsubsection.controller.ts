import { Controller, Get} from "@nestjs/common";
import { SubsectionService } from "./subsection.service";
import { ApiTags, ApiOperation, ApiResponse } from "@nestjs/swagger/dist";
import { Subsection } from "./entities/subsection.entity";

@ApiTags('subsection')
@Controller('/random_subsections')
export class RandSubsectionController{
    constructor(
        private _subsectionService: SubsectionService,
    ){}
    @ApiOperation({summary: 'Получение случайных разделов'})
    @ApiResponse({status: 200, type: [Subsection], })
    @Get()
    getRandom(){
        return this._subsectionService.getRandom();
    }
}