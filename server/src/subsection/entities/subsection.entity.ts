import { ApiProperty } from "@nestjs/swagger/dist";
import { Article } from "src/article/entities/article.entity";
import { Section } from "src/section/entities/section.entity";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class Subsection{

    @ApiProperty({example: 'xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx', description: 'Уникальный идентификатор'})
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ApiProperty({example: 'Виды коммуникационных средств', description: 'Название подраздела'})
    @Column()
    title: string;

    @ApiProperty({example: 'Всё про коммуникационные средства', description: 'Описание подраздела'})
    @Column({default: ''})
    description: string;

    @ApiProperty({example: 'xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx', description: 'Раздел, содержащий данный подраздел'})
    @ManyToOne(()=>Section, (section)=>section.subsections)
    @JoinColumn({name: 'parentSection_id'})
    parentSection: Section;

    @OneToMany(()=>Article, (article) => article.subsection, { onDelete: 'CASCADE'})
    articles: Article[]
}