import { Body, Controller, Delete, Get, Param, Post, UploadedFiles, UseInterceptors } from "@nestjs/common";
import { SubsectionService } from "./subsection.service";
import { CreateSubsectionDto } from "./dto/create-subsection.dto";
import { ApiTags, ApiOperation, ApiResponse } from "@nestjs/swagger/dist";
import { Subsection } from "./entities/subsection.entity";

@ApiTags('subsection')
@Controller('/subsections')
export class SubsectionController{
    constructor(
        private _subsectionService: SubsectionService,
    ){}

    @ApiOperation({summary: 'Создание подраздела'})
    @ApiResponse({status: 200, type: Subsection})
    @Post()
    create(@Body() dto: CreateSubsectionDto, id: string){
        return this._subsectionService.create(dto);
    }

    @ApiOperation({summary: 'Получение всех подразделов'})
    @ApiResponse({status: 200, type: [Subsection]})
    @Get()
    getAll(){
        return this._subsectionService.getAll();
    }

    @ApiOperation({summary: 'Получение одного подраздела по id'})
    @ApiResponse({status: 200, type: Subsection})
    @Get(':id')
    getOne(@Param('id') id: string){
        return this._subsectionService.getOne(id);
    }

    @ApiOperation({summary: 'Удаление одного подраздела по id'})
    @ApiResponse({status: 200, type: Subsection})
    @Delete(':id')
    delete(@Param('id') id: string){
        return this._subsectionService.delete(id);
    }
}