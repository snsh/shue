import { MigrationInterface, QueryRunner } from "typeorm";

export class Auto1699790903171 implements MigrationInterface {
    name = 'Auto1699790903171'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "articles" RENAME COLUMN "image_src" TO "image"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "articles" RENAME COLUMN "image" TO "image_src"`);
    }

}
