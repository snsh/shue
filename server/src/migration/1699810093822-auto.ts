import { MigrationInterface, QueryRunner } from "typeorm";

export class Auto1699810093822 implements MigrationInterface {
    name = 'Auto1699810093822'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "articles" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "subsection" ADD "description" character varying NOT NULL DEFAULT ''`);
        await queryRunner.query(`ALTER TABLE "section" ALTER COLUMN "description" SET DEFAULT ''`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "section" ALTER COLUMN "description" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "subsection" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "articles" ADD "description" character varying NOT NULL`);
    }

}
