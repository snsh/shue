import { MigrationInterface, QueryRunner } from "typeorm";

export class Auto1699643505387 implements MigrationInterface {
    name = 'Auto1699643505387'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "articles" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "title" character varying NOT NULL, "description" character varying NOT NULL, "image" character varying NOT NULL, "content" character varying NOT NULL, "author" character varying NOT NULL, "createdAt" date NOT NULL DEFAULT now(), "keyWords" character varying NOT NULL, "subsection_id" uuid, CONSTRAINT "PK_0a6e2c450d83e0b6052c2793334" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "subsection" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "title" character varying NOT NULL, "parentSection_id" uuid, CONSTRAINT "PK_aebbb84e28d5deec5bc853b9f0d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "section" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "title" character varying NOT NULL, "image" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_3c41d2d699384cc5e8eac54777d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("user_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "login" character varying NOT NULL, "password" character varying NOT NULL, "email" character varying NOT NULL, "name" character varying NOT NULL, "middlename" character varying NOT NULL, "surname" character varying NOT NULL, "city" character varying NOT NULL, "birthdate" date NOT NULL, "createdAt" date NOT NULL DEFAULT now(), "updatedAt" date NOT NULL DEFAULT now(), CONSTRAINT "UQ_a62473490b3e4578fd683235c5e" UNIQUE ("login"), CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_758b8ce7c18b9d347461b30228d" PRIMARY KEY ("user_id"))`);
        await queryRunner.query(`ALTER TABLE "articles" ADD CONSTRAINT "FK_b25938054d48e27e08201cc8c2a" FOREIGN KEY ("subsection_id") REFERENCES "subsection"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "subsection" ADD CONSTRAINT "FK_a0eaab6406cfc6fc2944fa969b6" FOREIGN KEY ("parentSection_id") REFERENCES "section"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "subsection" DROP CONSTRAINT "FK_a0eaab6406cfc6fc2944fa969b6"`);
        await queryRunner.query(`ALTER TABLE "articles" DROP CONSTRAINT "FK_b25938054d48e27e08201cc8c2a"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "section"`);
        await queryRunner.query(`DROP TABLE "subsection"`);
        await queryRunner.query(`DROP TABLE "articles"`);
    }

}
