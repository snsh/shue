import { ApiProperty } from "@nestjs/swagger/dist";
import { Subsection } from "src/subsection/entities/subsection.entity";

export class CreateArticleDto{
    
    @ApiProperty({example: 'Что делать?', description: 'Название статьи'})
    title: string;
      
    @ApiProperty({example: 'Иван Петрович Молодцов', description: 'Имя автора'})
    author: string;

    @ApiProperty({example: 'уход, ОВЗ, пациенты', description: 'Список ключевых слов'})
    keyWords: string;

    @ApiProperty({example: 'xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx', description: 'Идентификатор подраздела, содержащего данную статью'})
    subsection: Subsection;
}