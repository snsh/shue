import { BadRequestException, Injectable } from "@nestjs/common";
import { CreateArticleDto } from "./dto/create-article.dto";
import { FileService, FileType } from "src/file/file.service";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Article } from "./entities/article.entity";

@Injectable()
export class ArticleService{
    
    constructor(@InjectRepository(Article) private _articlesRepository: Repository<Article>,
        private _fileService: FileService
    ){}
    
    async create(dto: CreateArticleDto, content): Promise<Article>{
        const contentHtml = await this._fileService.create(FileType.CONTENT, content);
        const newArticle = {
            ...dto, 
            content: contentHtml
        };
        if(!newArticle) throw new BadRequestException('Something went wrong...')
        return this._articlesRepository.save(newArticle);
    }

    async getAll(): Promise<Article[]> {
        const articles = await this._articlesRepository.find({
            select:{
                id: true,
                title: true,
            }
        });
        return articles;
    }

    async getOne(id: string): Promise<Article>{
        const article = await this._articlesRepository.findOne({
            select: {
                id: true,
                title: true, 
                createdAt: true,
                content: true,
                author: true,
            },
            where: {id}
        });
        return article;
    }

    async delete(id: string): Promise<void>{
        const article = await this._articlesRepository.delete(id);
    }
}