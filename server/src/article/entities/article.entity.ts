import { ApiProperty } from "@nestjs/swagger/dist";
import { Subsection } from "src/subsection/entities/subsection.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity("articles")
export class Article{
    
    @ApiProperty({example: 'xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx', description: 'Уникальный идентификатор'})
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ApiProperty({example: 'Что делать?', description: 'Название статьи'})
    @Column()
    title: string;

    @ApiProperty({example: '<ol>Текст</ol><ul><li>Элемент 1<li><li>Элемент 2<li><li>Элемент 3<li>', description: 'Html разметка статьи'})
    @Column()
    content: string;

    @ApiProperty({example: 'Иван Петрович Молодцов', description: 'Имя автора'})
    @Column()
    author: string;

    @ApiProperty({example: 'YYYY-MM-DD', description: 'Дата добавления статьи'})
    @CreateDateColumn({type: "date"})
    createdAt: Date;

    @ApiProperty({example: 'уход, ОВЗ, пациенты', description: 'Список ключевых слов'})
    @Column()
    keyWords: string;

    @ApiProperty({example: 'xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx', description: 'Идентификатор подраздела, содержащего данную статью'})
    @ManyToOne(()=> Subsection, subsection => subsection.articles)
    @JoinColumn({name: "subsection_id"})
    subsection: Subsection;

}
