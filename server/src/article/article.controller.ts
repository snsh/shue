﻿import { Body, Controller, Delete, Get, Param, Post, UploadedFiles, UseInterceptors} from "@nestjs/common";
import { ArticleService } from "./article.service";
import { CreateArticleDto } from "./dto/create-article.dto";
import { Article } from "./entities/article.entity";
import {FileFieldsInterceptor} from "@nestjs/platform-express";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger/dist";

@ApiTags("articles")
@Controller( '/articles')
export class ArticleController {
    constructor(private _articleService: ArticleService){}

    @ApiOperation({summary: 'Создание статьи'})
    @ApiResponse({status: 200, type: Article})
    @Post()
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'content', maxCount: 1 }
      ]))
    create(
        @UploadedFiles() files: { content?: Express.Multer.File[]}, 
        @Body() dto: CreateArticleDto, id: number){
        return this._articleService.create(dto, files.content[0]);
    }

    @ApiOperation({summary: 'Получение всех статей'})
    @ApiResponse({status: 200, type: [Article]})
    @Get()
    getAll() {
        return this._articleService.getAll();
    }

    @ApiOperation({summary: 'Получение одной статьи по id'})
    @ApiResponse({status: 200, type: Article})
    @Get(':id')
    getOne(@Param('id') id: string){
        return this._articleService.getOne(id);
    }

    @ApiOperation({summary: 'Удаление одной статьи по id'})
    @ApiResponse({status: 200})
    @Delete(':id')
    delete(@Param('id') id: string){
        return this._articleService.delete(id);
    }
}



