import { Body, Controller, Delete, Get, Param, Post } from "@nestjs/common";
import { CreateUserDto } from "./dto/create-user.dto";
import { UserService } from "./user.service";


@Controller('/users')
export class UserController{
    
    constructor(private _userService: UserService){}

    @Post()
    create(@Body()dto: CreateUserDto){
        return this._userService.create(dto);
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this._userService.getOne(id);
    }

    @Get()
    getAll(){
        return this._userService.getAll();
    }

    @Delete(':id')
    delete(@Param('id') id: number){
        return this._userService.delete(id);
    }
}