import { Injectable } from "@nestjs/common";
//import { User, UserDocument } from "./schemas/user.schema";
//import { InjectModel } from "@nestjs/mongoose";
//import { Model } from "mongoose";
import { CreateUserDto } from "./dto/create-user.dto";
import { User } from "./entities/user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";


@Injectable()
export class UserService{
    
    constructor(@InjectRepository(User) private _userRepository: Repository<User>){}

    async create(dto: CreateUserDto): Promise<User>{
        const user = await this._userRepository.create({...dto});
        return this._userRepository.save(user);
    }

    async getOne(id: number): Promise<User>{
        const user = await this._userRepository.findOneBy({id});
        return user;
    }

    async getAll(): Promise<User[]>{
        const users = await this._userRepository.find();
        return users;
    }

    async delete(id: number): Promise<void>{
        const user = await this._userRepository.delete(id)
    }
}