export class CreateUserDto{
    login: string;
    password: string;
    email: string;
    name: string;
    middlename: string;
    surname: string;
    city: string;
    birthdate: Date;
}