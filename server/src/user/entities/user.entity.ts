import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class User{
    @PrimaryGeneratedColumn("uuid", {
        name: "user_id",
    })
    id: number;
 
    @Column({
        unique: true,
    })
    login: string;

    @Column()
    password: string;

    @Column({
        unique: true,
    })
    email: string;

    @Column()
    name: string;

    @Column()
    middlename: string;

    @Column()
    surname: string;

    @Column()
    city: string;

    @Column({type: "date"})
    birthdate: Date;

    @CreateDateColumn({type: "date"})
    createdAt: Date;

    @UpdateDateColumn({type: "date"})
    updatedAt: Date;

    @Column()
    testColumn: string;
}